import pyxel as pyx
from random import randint 

# constantes
## couleurs
NOIR = 0
ROUGE = 8
BLANC = 7
## dimensions
LG_ECRAN = HT_ECRAN = 256
RAYON_BUL = 10
LG_PERSO = HT_PERSO = 10
# variables
## bulle folle
xbul = 10       # position horiz dela bulle
ybul = 10       # position vertic de la bulle
vxbul = 5       # déplacement horiz de la bulle
vybul = 5       # déplacement vertic de la bulle

positions_passees = []
## personage
xperso = LG_ECRAN // 2
yperso = HT_ECRAN // 2


def afficher_trainee():
    """
    affiche une queue de comète derrière la bulle
    """
    for i, (x, y) in enumerate(positions_passees):
        pyx.circ(x, y, RAYON_BUL - i, ROUGE)


def draw():
    pyx.cls(NOIR)
    pyx.circ(xbul, ybul, RAYON_BUL, ROUGE)                  # bulle folle
    pyx.rect(xperso, yperso, LG_PERSO, HT_PERSO, BLANC)     # personnage
    afficher_trainee()


def update():
    global xbul, ybul, vxbul, vybul, xperso, yperso
    # rebonds bulle
    if xbul < RAYON_BUL:
        vxbul *= -1                          # rebond
        xbul = RAYON_BUL                     # éviter la capture par le mur
    elif xbul > LG_ECRAN - RAYON_BUL:
        vxbul *= -1                                          
        xbul = LG_ECRAN - RAYON_BUL 
    if ybul < RAYON_BUL:
        vybul *= -1
        ybul = RAYON_BUL
    elif ybul > HT_ECRAN - RAYON_BUL:
        vybul *= -1
        ybul = HT_ECRAN - RAYON_BUL

    # déplacement bulle
    xbul += vxbul
    ybul += vybul

    # facteur aléatoire
    if pyx.frame_count % 10 == 0:
        d = randint(-1, 1)
        vxbul += d
        vybul -= d


    # traînée
    if pyx.frame_count % 2 == 0:
        positions_passees.insert(0, (xbul, ybul))
        if len(positions_passees) > 10:
            positions_passees.pop()
    
    # déplacements personnage
    if 0 < pyx.mouse_x < LG_ECRAN-LG_PERSO and\
            0 < pyx.mouse_y < HT_ECRAN-HT_PERSO:
                xperso = pyx.mouse_x
                yperso = pyx.mouse_y



pyx.init(LG_ECRAN, HT_ECRAN, title="Éviter la bulle folle")
pyx.run(update, draw)
    




