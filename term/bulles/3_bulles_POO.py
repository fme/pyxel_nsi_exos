import pyxel
from random import randint
ROUGE = 8
LARG = 128
HAUT = 128


class Bulle():
    """
    """
    def __init__(self, xbulle, ybulle, rbulle, coulbulle, vbulle=[0, 0]):
        self.x = xbulle
        self.y = ybulle
        self.r = rbulle
        self.coul = coulbulle
        self.vect = vbulle

    def dessiner(self):
        pyxel.circ(self.x, self.y, self.r, self.coul)

    def grossir(self, de):
        self.r += de

    def deplacer(self):
        self.x += self.vect[0]
        self.y += self.vect[1]
        # rebonds sur les bords
        if not self.r < self.x < LARG - self.r:
            self.vect[0] = -self.vect[0]
            self.x += self.vect[0]
        if not self.r < self.y < HAUT-self.r:
            self.vect[1] = - self.vect[1]
            self.y += self.vect[1]


bulles = [Bulle(x, HAUT // 2, 5, randint(0, 15), [randint(-3, 3),
                                                  randint(-3, 3)])
          for x in range(0, 120, 20)]


def draw():
    pyxel.cls(0)
    for b in bulles:
        b.dessiner()


def update():
    for b in bulles:
        b.deplacer()


pyxel.init(LARG, HAUT, title="Bulles POO")
pyxel.run(update, draw)
