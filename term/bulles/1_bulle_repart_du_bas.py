# CONSIGNE : APRÈS AVOIR DISPARU EN HAUT, LA BULLE DOIT REPARTIR DU BAS
# VERSION CORRIGÉE
import pyxel
ROUGE = 8
LARG = 128
HAUT = 128

xbulle = LARG / 2       # au milieu
ybulle = HAUT
rbulle = 8

def draw():
    pyxel.cls(0)
    pyxel.circ(xbulle, ybulle, rbulle, ROUGE)

def update():
    global ybulle
    # partie à compléter entièrement dans la version élève
    if ybulle < -rbulle:          # bulle disparaît en haut <=> ybulle < -r
        ybulle = HAUT + rbulle    # elle réapparaît en bas
    else:
        ybulle = ybulle - 1
    # fin partie à compléter



       
# ne pas modifier ce qui suit
pyxel.init(LARG, HAUT, title="Bulle")
pyxel.run(update, draw)
