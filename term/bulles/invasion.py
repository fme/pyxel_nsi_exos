import pyxel
from random import randint, random, choice
from module_bulle import Bulle

RAYON = 8
BLANC = 7
ROUGE = 8
LARG, HAUT = 256, 256
VITESSEMAX = 10
APPROX = 3



class Phagocyte():
    """
    """
    def __init__(self, rayon, initx, inity):
        self.ray = rayon
        self.x, self.y = initx, inity

    def suivre_souris1(self):
        self.x, self.y = pyxel.mouse_x, pyxel.mouse_y

    def suivre_souris_dist(self, lenteur):
        pyxel.mouse(True)
        vect = (pyxel.mouse_x - self.x, pyxel.mouse_y - self.y)
        self.x += vect[0] * (1/lenteur)
        self.y += vect[1] * (1/lenteur)

    def dessiner(self):
        pyxel.circb(self.x, self.y, self.ray, BLANC)

    def attraper(self, proie) -> bool:
        """
        proie est une bulle
        """
        return abs(self.x-proie.x) < APPROX and abs(self.y-proie.y) < APPROX


class Bulle_num(Bulle):
    """
    bulle porteuse d'un numéro
    """
    def __init__(self, bullex, bulley, bray, bcoul, bvect, bnum: int):
        Bulle.__init__(self, bullex, bulley, bray, bcoul, bvect)
        self.num = bnum

    def dessiner(self):
        Bulle.dessiner(self)
        pyxel.text(self.x, self.y, str(self.num), 0)


class Infection():
    """
    """
    def __init__(self, croissance):
        """
        """
        # définir des limites pour l'argument croissance
        self.probadiv = croissance
        xvect = 2 * (-1)**randint(0, 1)
        yvect = 2 * (-1)**randint(0, 1)
        self.bulles = [Bulle_num(LARG // 2,
                                 HAUT // 2,
                                 RAYON,
                                 ROUGE,
                                 [xvect, yvect],
                                 1)]
        self.phagocyte = Phagocyte(RAYON + 2, LARG // 2, HAUT // 2)

        pyxel.init(LARG, HAUT, title="Invasion")
        pyxel.run(self.modifier, self.dessiner)
        pyxel.mouse(True)

    def dessiner(self):
        pyxel.cls(0)
        for b in self.bulles:
            b.dessiner()
        self.phagocyte.dessiner()

    def modifier(self):
        if not self.bulles:
            print("gagné !")
            pyxel.quit()
            
        for b in self.bulles:
            b.deplacer()
            b.rebondir_bords(LARG, HAUT)

        self.phagocyte.suivre_souris_dist(10)
        if self.phagocyte.attraper(b):
            print("attrapé {}".format(b.num))
            self.bulles.pop()

        # division d'une cellule ?
        if randint(1, self.probadiv) == 1:
            self.mitose()

    def mitose(self):   # division cellulaire du sommet de pile
        """
        """
        frein = .5
        mama = self.bulles.pop()
        self.bulles.append(Bulle_num(mama.x,
                                     mama.y,
                                     mama.r,
                                     randint(2, 15),
                                     [mama.vect[1] * frein,
                                      -mama.vect[0] * frein],
                                     2 * mama.num))
        self.bulles.append(Bulle_num(mama.x,
                                     mama.y,
                                     mama.r,
                                     randint(2, 15),
                                     [-mama.vect[1] * frein,
                                      mama.vect[0] * frein],
                                     2 * mama.num + 1))


Infection(100)


