"""
CONSIGNE : APRÈS AVOIR DISPARU EN HAUT, LA BULLE DOIT REPARTIR DU BAS
VERSION CORRIGÉE
1/ reprendre 1 bulle et intégrer le code déplacement de la bulle dans une 
fonction déplacer_bulle qui agit sur la var globale
2/ représenter 1 bulle par une liste ou un dict de x et y et r. Modifier le 
programme en conséquznce
3/ Quels sont les attributs pertinents pour représenter une bulle ? 
4/ On ne peut pas représenter le sens de déplacement : ajouter un vecteur
5/ Voir le fichier exemple 1bulle_POO et animer plusieurs bulles 

"""
import pyxel
ROUGE = 8
r = 10
LARG = 128
HAUT = 128
xbulle = LARG / 2       # au milieu
ybulle = HAUT

bulle1 = [LARG * 2/3, HAUT]
bulle2 = [LARG * 1/3, 0]
bulles = (bulle1, bulle2)

def deplacer_bulle(bulle):
    """
    modifie les variables globales
    """  
    if bulle[1] < -r:          # bulle disparaît en haut <=> bulle[1] < -r
        bulle[1] = HAUT + r    # elle réapparaît en bas
    else:
        bulle[1] = bulle[1] - 1


def draw():
    pyxel.cls(0)
    for b in bulles:
        pyxel.circ(b[0], b[1], r, ROUGE)

def update():
    for b in bulles:
        deplacer_bulle(b)


       
# ne pas modifier ce qui suit
pyxel.init(LARG, HAUT, title="Bulle")
pyxel.run(update, draw)
