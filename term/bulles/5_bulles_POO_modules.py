import pyxel
from random import randint
from math import sqrt 
from module_point import Point
from module_vecteur import Vecteur

ROUGE = 8
LARG = 128
HAUT = 128


class Bulle():
    """
    DOCUMENTER LA CLASSE BULLE
    """
    def __init__(self, centrebulle: Point, rbulle, coulbulle, vbulle: Vecteur):
        self.centre = centrebulle
        self.r = rbulle
        self.coul = coulbulle
        self.vect = vbulle

    def dessiner(self):
        """
        dessine la bulle. Pas d'argument.
        """
        pyxel.circ(self.centre.x, self.centre.y, self.r, self.coul)

    def grossir(self, de):
        """
        fait grossir la bulle
        argument:
        de : un int, nombre de pixels en plus ou en moins
        À FAIRE : CONTRÔLER LA VALEUR DE L'ARGUMENT POUR ÉVITER
        QUE LA BULLE DISPARAISSE OU ENVAHISSE L'ÉCRAN
        """
        self.r += de
    
    def rebondir_bords(self):
        """
        modifie la valeur du vecteur de déplacement pour simuler un rebond
        sur les bords de l'écran
        pas d'argument
        """
        if not self.r < self.centre.x < LARG - self.r:
            self.vect.x = -self.vect.x
            self.centre.x += self.vect.x
        if not self.r < self.centre.y < HAUT-self.r:
            self.vect.y = - self.vect.y
            self.centre.y += self.vect.y

    def deplacer(self):
        """
        déplace la bulle en ajoutant son vecteur de déplacement à son centre
        actuel.
        Pas d'argument
        """
        self.centre.x += self.vect.x
        self.centre.y += self.vect.y
        self.rebondir_bords()

    def deplacer_de(self, vect: Vecteur):
        """
        dépace la bulle d'un vecteur fourni
        Argument : vect, une instance de la classe Vecteur
        UTILISER LA MÉTHODE TRANSLATION DE LA CLASSE POINT
        """
        self.centre.translation(vect)
        self.rebondir_bords()

    def distance(self, autre_bulle):
        return self.centre.distance(autre_bulle.centre)

    def collision(self, autre_bulle):
        return self.distance(autre_bulle) < self.r + autre_bulle.r

    def suivre_souris(self):
        self.centre.x = pyxel.mouse_x
        self.centre.y = pyxel.mouse_y
        

bulles = [Bulle(Point(x, HAUT // 2), 
                5, 
                randint(1, 14), 
                Vecteur(randint(-3, 3), randint(-3, 3))) 
          for x in range(0, 120, 20)]

ma_bulle = Bulle(Point(LARG//2, HAUT//2), 6, 15, Vecteur(0, 0))

def draw():
    pyxel.cls(0)
    for b in bulles:
        b.dessiner()
    ma_bulle.dessiner()

def update():
    for b in bulles:
        b.deplacer()
    ma_bulle.suivre_souris()
    


pyxel.init(LARG, HAUT, title="Bulles POO_v2")
pyxel.run(update, draw)
