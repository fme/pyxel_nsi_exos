import pyxel
from random import randint
from math import sqrt
ROUGE = 8
LARG = 128
HAUT = 128

class Vecteur():
    """
    représente un vecteur en coordonnées cartésiennes
    """
    def __init__(self, xvect, yvect):
        self.x = xvect
        self.y = yvect

    def norme(self):
        return sqrt(self.x**2 + self.y**2)


class Bulle():
    """
    """
    def __init__(self, xbulle, ybulle, rbulle, coulbulle, vbulle: Vecteur):
        self.x = xbulle
        self.y = ybulle
        self.r = rbulle
        self.coul = coulbulle
        self.vect = vbulle

    def dessiner(self):
        pyxel.circ(self.x, self.y, self.r, self.coul)

    def grossir(self, de):
        self.r += de
    
    def rebondir(self):
        # rebonds sur les bords
        if not self.r < self.x < LARG - self.r:
            self.vect.x = -self.vect.x
            self.x += self.vect.x
        if not self.r < self.y < HAUT-self.r:
            self.vect.y = - self.vect.y
            self.y += self.vect.y

    def deplacer(self):
        self.x += self.vect.x
        self.y += self.vect.y
        self.rebondir()

    def deplacer_de(self, vect: Vecteur):
        self.x += vect.x
        self.y += vect.y
        self.rebondir()

    def distance(self, autre_bulle):
        return sqrt((self.x - autre_bulle.x)**2 +
                    (self.y - autre_bulle.y)**2)

    def collision(self, autre_bulle):
        return self.distance(autre_bulle) < self.r + autre_bulle.r

    def suivre_souris(self):
        self.x = pyxel.mouse_x
        self.y = pyxel.mouse_y

bulles = [Bulle(x, 
                HAUT // 2, 5, 
                randint(1, 14), 
                Vecteur(randint(-3, 3), randint(-3, 3))) 
          for x in range(0, 120, 20)]

ma_bulle = Bulle(LARG//2, HAUT//2, 6, 15, Vecteur(0, 0))

def draw():
    pyxel.cls(0)
    for b in bulles:
        b.dessiner()
    ma_bulle.dessiner()

def update():
    for b in bulles:
        b.deplacer()
    ma_bulle.suivre_souris()


pyxel.init(LARG, HAUT, title="Bulles POO_v2")
pyxel.run(update, draw)
