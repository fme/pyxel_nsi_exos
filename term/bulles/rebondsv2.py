"""
Exercice pour les term : réécrire en POO
avec une classe Bulle, une classe vecteur

"""

import pyxel, math, random

la = ho = 400
r = 10
nbbulles = 7
# création des bulles à des endroits aléatoires de sorte qu'elles
# ne se recouvrent pas.
g = [ (i, j) for i in range(la // r) for j in range(ho // r) ]
posb = random.sample(g, nbbulles)
# positions 
x = [bu[0] * r for bu in posb]
y = [bu[1] * r for bu in posb]
# vecteurs de vitesse
vx = [random.randint(-100, 100) / 10 for _ in range(nbbulles)]
vy = [random.randint(-100, 100) / 10 for _ in range(nbbulles)]
# couleurs
c = [random.randint(1, 15) for _ in range(nbbulles)]

assert len(x) == len(y) == len(vx) == len(vy) == len(c)

def dessine():
    pyxel.cls(0)
    for i in range(nbbulles):
        pyxel.circ(x[i], y[i], r , c[i])

def rebond(i):  # murs
    if x[i] <= r:
        vx[i] *= -1
        x[i] = r        # évite boule coincée dans le mur si rebond et choc simultanés 
    elif x[i] >= la - r:
        vx[i] *= -1
        x[i] = la - r
    if y[i] <= r:
        vy[i] *= -1
        y[i] = r
    elif y[i] >= ho - r:
        vy[i] *= -1
        y[i] = ho - r


def distance(x1, y1, x2, y2):
    return math.sqrt( (x1 - x2)**2 + (y1 - y2)**2 )

def chocbulle(bu1,bu2):
    dist = (x[bu1] - x[bu2]) **2 + (y[bu1] - y[bu2]) **2 # and not choque[bu1][bu2]:
    if dist <= r * r * 4:
        if dist < r * r * 4:
            # si les deux boules se sont pénétrées il faut que chacune recule
            # d'une quantité qu'on calcule par le théorème de Thalès (je crois)
            ra = 2 * r / distance(x[bu1] - vx[bu1], y[bu1] - vy[bu1], 
                                  x[bu2] - vx[bu2], y[bu2] - vy[bu2])
            # reculs à effectuer
            x[bu1], y[bu1] = x[bu1] - vx[bu1] * ra, y[bu1] - vy[bu1] * ra
            x[bu2], y[bu2] = x[bu2] - vx[bu2] * ra, y[bu2] - vy[bu2] * ra
        # axe de symétrie du choc
        wx, wy = x[bu2] - x[bu1], y[bu2] - y[bu1] 
        # vitesse centre de gravité du système des deux boules
        vgx = (vx[bu1] + vx[bu2]) / 2
        vgy = (vy[bu1] + vy[bu2]) / 2
        # coefficient du à la projection orthogonale sur l'axe de symétrie
        k = ( ( vx[bu1] - vgx ) * wx + ( vy[bu1] - vgy ) * wy ) / ( wx**2 + wy**2 ) 
        vx[bu1] -= 2 * k * wx
        vy[bu1] -= 2 * k * wy
        vx[bu2] += 2 * k * wx
        vy[bu2] += 2 * k * wy 
        #choque[bu1][bu2] = 2
        
def actualise():
    for bu1 in range(nbbulles):
        for bu2 in range(bu1+1, nbbulles):
            chocbulle(bu1, bu2)
        
    for bu in range(nbbulles):
        rebond(bu) 
        x[bu] += vx[bu]
        y[bu] += vy[bu]
    
pyxel.init(la, ho, title="chocs et rebonds")
pyxel.run(actualise, dessine)
