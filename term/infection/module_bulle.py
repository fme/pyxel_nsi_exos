"""
Module pour importer la classe Bulle
"""
import pyxel


class Bulle():
    """
    """
    def __init__(self, xbulle, ybulle, rbulle, coulbulle, vbulle=[0, 0]):
        self.x = xbulle
        self.y = ybulle
        self.r = rbulle
        self.coul = coulbulle
        self.vect = vbulle

    def dessiner(self):
        pyxel.circ(self.x, self.y, self.r, self.coul)

    def grossir(self, de):
        self.r += de

    def deplacer(self):
        """
        applique le vecteur self.vect à la position de la bulle
        définie par self.x, self.y
        """
        self.x += self.vect[0]
        self.y += self.vect[1]


    def deplacer_de(self, vectx, vecty):
        """
        applique le vecteur self.vect à la position de la bulle
        définie par self.x, self.y
        """
        self.x += vectx
        self.y += vecty

    def rebondir_bords(self, larg, haut):
        # rebonds sur les bords
        if not self.r < self.x < larg - self.r:
            self.vect[0] = -self.vect[0]
            self.x += self.vect[0]
        if not self.r < self.y < haut-self.r:
            self.vect[1] = - self.vect[1]
            self.y += self.vect[1]
