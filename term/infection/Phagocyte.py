"""
Module contenant la classe Phagocyte
On recopie presque la classe Bulle puisqu'en term NSI on ne fait pas l'héritage.
TODO faire une classe bactérie avec les attributs de bulle + mangée ou pas
"""
import pyxel
from module_bulle import Bulle
from random import randint

LARG, HAUT = 256, 256
MIH, MIV = LARG / 2, HAUT / 2
PRAYON, PCOUL = 8, 8
RBACT = 6
DENS = 100
FREIN = 10


class Phagocyte():
    """
    il ne bouge pas du milieu. C'est l'espace autour de lui qui bouge !
    """   
    def __init__(self, rbulle, coulbulle):
        self.r = rbulle
        self.coul = coulbulle
        self.proies = 0

    def dessiner(self):
        pyxel.circb(MIH, MIV, self.r, 7)

    def grossir(self, de):
        self.r += de


class Espace():
    """
    représente l'espace du jeu avec les bactéries
    """
    def __init__(self, largesp, hautesp, nbbact):
        """
        """
        self.larg = largesp
        self.haut = hautesp
        self.origx = -self.larg / 2
        self.origy = -self.haut / 2
        self.nbbact = nbbact
        self.bact = []
        #  disposition des bactéries de manière aléatoire, sur une grille pour
        # éviter qu'elles se chevauchent.
        self.grille = [[randint(1, DENS) == DENS
                        for _ in range(self.larg // RBACT)]
                       for _ in range(self.haut // RBACT)]
        for li in range(len(self.grille)):
            for co in range(len(self.grille[0])):
                if self.grille[li][co]:
                    self.bact.append([Bulle(self.origx + li * RBACT,
                                            self.origy + co * RBACT,
                                            RBACT,
                                            randint(1, 15),
                                            [0, 0]),
                                      True])

    def translation(self, vectx, vecty) -> bool:
        """
        translation de vecteur vect de notre espace de jeu pour simuler
        le mouvement
        vectx = déplacement horizontal
        vecty = déplacement vertical
        Uniquement si on ne franchit pas les murs
        """
        if self.origx - vectx + self.larg > MIV and\
           self.origx - vectx < MIV and\
           self.origy - vecty < MIH and\
           self.origy - vecty + self.haut > MIH:
            # déplacer le décor (les bactéries)
            for bact, visi in self.bact:
                bact.deplacer_de(-vectx, -vecty)
            # déplacer le point d'origine
            self.origx -= vectx
            self.origy -= vecty
            return True
        else:  # cogné dans un mur
            return False
          
    def dessiner(self):
        """
        """
        # bactéries
        for bact, visi in self.bact:
            if visi:
                bact.dessiner()
        # murs
        pyxel.rectb(self.origx, self.origy, self.larg, self.haut, 7)


class Jeu():
    """
    """
    def __init__(self, flarg, fhaut):
        self.espace = Espace(flarg * 4, fhaut * 4, 29)
        self.phago = Phagocyte(PRAYON, PCOUL)
    
        pyxel.init(flarg, fhaut, title="Invasion")
        pyxel.run(self.modifier, self.dessiner)

    def modifier(self):
        pyxel.mouse(True)
        self.espace.translation((pyxel.mouse_x - MIV) / FREIN,
                                (pyxel.mouse_y - MIH) / FREIN)
        # cette évolution doit être gérée ici car il faut tenir
        # compte de la grosseur du phagocyte
        approx = self.phago.r / 3
        for elem in self.espace.bact:
            if elem[1] and \
               MIH - approx < elem[0].x < MIH + approx and \
               MIV - approx < elem[0].y < MIV + approx:
                elem[1] = False
                self.espace.nbbact -= 1
                self.phago.grossir(1)
                print(self.espace.nbbact)
                break  # pas deux à la fois
 
    def dessiner(self):
        pyxel.cls(0)
        self.espace.dessiner()
        self.phago.dessiner()


Jeu(LARG, HAUT)
