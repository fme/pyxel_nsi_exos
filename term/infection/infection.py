"""
Module contenant la classe Phagocyte
On recopie presque la classe Bulle puisqu'en term NSI on ne fait pas l'héritage.

la souris semble traîner le phagocyte mais en fait
c'est juste le point d'origine des coordonnées de
essin qui bouge
"""
import pyxel
from random import randint

PRAYON, PCOUL = 8, 8
RBACT = 6
DENS = 100
FREIN = 10


class Phagocyte():
    """
    il ne bouge pas du milieu. C'est l'espace autour de lui qui bouge !
    """  
    def __init__(self, rbulle, coulbulle):
        self.r = rbulle
        self.coul = coulbulle
        self.proies = 0
        self.x = pyxel.width / 2
        self.y = pyxel.height / 2

    def dessiner(self):
        pyxel.circb(self.x, self.y, self.r, 7)

    def grossir(self, de):
        self.r += de


class Bacterie():
    """
    """
    def __init__(self, xbact, ybact, rbact, coulbact):
        self.x = xbact
        self.y = ybact
        self.r = rbact
        self.visible = True
        self.coul = coulbact

    def dessiner(self, origx, origy):
        """
        dessine en tenant compte du point d'origine des coordonnées
        """
        pyxel.circ(origx + self.x, origy + self.y, self.r, self.coul)

    def grossir(self, de):
        self.r += de

    def deplacer(self, dex, dey):
        """
        applique le vecteur (dex, dey) à  la position de la bactérie
        """
        self.x, self.y = self.x + dex, self.y + dey


class Jeu():
    """
    """
    def __init__(self, cote_grille, larg_ecran, haut_ecran):
        self.larg = self.haut = RBACT * cote_grille
        self.centrex, self.centrey = larg_ecran / 2, haut_ecran / 2
        # il faut positionner l'écran au milieu de l'espace, qui est carré
        self.origx = -(self.larg - larg_ecran) / 2
        self.origy = -(self.haut - haut_ecran) / 2
        self.bact = []

        #  disposition des bactéries de manière aléatoire, sur une grille 
        self.grille = [[randint(1, DENS) == DENS
                        for _ in range(cote_grille)]
                       for _ in range(cote_grille)]
        for li in range(cote_grille):
            for co in range(cote_grille):
                if self.grille[li][co]:
                    self.bact.append(Bacterie(li * RBACT,
                                              co * RBACT,
                                              RBACT,
                                              randint(1, 15)))
        pyxel.init(larg_ecran, haut_ecran, title="Invasion")
        self.phago = Phagocyte(PRAYON, PCOUL)
        pyxel.run(self.modifier, self.dessiner)

    def modifier(self):
        # mouvements du phagocyte
        pyxel.mouse(True)
        vectx = (pyxel.mouse_x - self.centrex) / FREIN
        vecty = (pyxel.mouse_y - self.centrey) / FREIN
        # on vérifie qu'on ne sort pas du terrain
        if self.origx - vectx + self.larg - self.phago.r > self.centrex and\
           self.origx - vectx + self.phago.r < self.centrex and\
           self.origy - vecty + self.phago.r < self.centrey and\
           self.origy - vecty + self.haut - self.phago.r > self.centrey:
            self.origx -= vectx
            self.origy -= vecty
        # ingestion des bactéries
        approx = self.phago.r / 3
        for bact in self.bact:
            if bact.visible and \
               self.centrex - approx < self.origx + bact.x < self.centrex + approx and \
               self.centrey - approx < self.origy + bact.y < self.centrey + approx:
                bact.visible = False
                self.phago.grossir(1)
                break  # pas deux à la fois
 
    def dessiner(self):
        pyxel.cls(0)
        pyxel.rectb(self.origx, self.origy, self.larg, self.haut, 7)
        for bact in self.bact:
            if bact.visible:
                bact.dessiner(self.origx, self.origy)
        self.phago.dessiner()


Jeu(100, 256, 256)
