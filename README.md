pyxel_nsi
====


## pyxel_prem

Activités avec pyxel pour les premières NSI.

* simulation de listes
* simulation de tri par insertion

## pyxel_term

Parcours de découverte du module pyxel pour les terminales

Bulles : animation de bulles de couleur qui doivent rebondir
sur les murs

Fractales : dessins  de fractales

* statiques avec une fonction `draw` récursive pour travailler avec la récursion. 
* dynamiques avec une pile ou une file pour stocker les composants et les dessiner peu à peu. L'idée est de travailler sur ces structures de données. 

Versions corrigées
