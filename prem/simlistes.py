"""
base pour des « jeux » d'algorithmique sur des listes
"""

from random import randint
import pyxel
# CONSTANTES
LARG = 256
HAUT = 128
COTE = 8                  # coté d'un élément
DEC = 2                   # décalage pour l'étiquette
SELECTB = pyxel.MOUSE_BUTTON_LEFT
VPOS = HAUT // 2
VPOS_INDICES = VPOS + 10

# représentation d'une liste
nbelm = 8                # nombre d'éléments dans la liste
listx = [i * LARG / nbelm + 1 for i in range(nbelm)]
listy = [VPOS] * nbelm
listv = [str(randint(0, 9)) for _ in range(nbelm)]

# élément déplaçable
mob = False
mobx = 0
moby = 0
mobv = False


def point_vers_index(px, py):
    """
    renvoie l'index de l'élément de liste contenant le point (px, py) ou
    None si le point n'est pas au bon endroit
    """
    if VPOS - COTE <= py <= VPOS + COTE:
        return int(px // (LARG / nbelm))


def mob_suit_souris():
    global mobx, moby
    mobx, moby = pyxel.mouse_x, pyxel.mouse_y


def clique_gauche():
    """
    suite à l'appui du bouton gauche de la souris, créer un élément mobile,
    et lui donner la valeur de l'élément de liste où se trouve la souris.
    """
    global mob, mobv
    mob = True
    mobv = listv[point_vers_index(pyxel.mouse_x, pyxel.mouse_y)]
    mob_suit_souris()
    pyxel.mouse(False)


def clique_droit():
    """
    suite à l'appui du bouton droit de la souris, désactiver l'élément mobile
    et écrire sa valeur dans l'élément de liste où se trouvait la souris.
    """
    global mob
    listv[point_vers_index(mobx, moby)] = mobv
    mob = False
    pyxel.mouse(True)


def dessine_mob():
    pyxel.text(mobx + DEC, moby + DEC, str(mobv), 7)


def dessine_liste():
    for i in range(nbelm):
        pyxel.rect(listx[i], listy[i], COTE, COTE, 12)
        pyxel.text(listx[i] + DEC, listy[i] + DEC, str(listv[i]), 0)
        pyxel.text(listx[i] + DEC, VPOS_INDICES, str(i), 11)


def modifie_liste():
    if pyxel.btnr(pyxel.MOUSE_BUTTON_LEFT):
        clique_gauche()
    if pyxel.btnr(pyxel.MOUSE_BUTTON_RIGHT):
        clique_droit()


def texte_explicatif():
    pyxel.text(2, 2, "left click : catch and move selected value", 7)
    pyxel.text(2, 12, "right click : put and write value at selected place", 7)

def dessine():
    """
    dessine les éléments graphiques
    """
    pyxel.cls(0)
    texte_explicatif()
    dessine_liste()
    if mob is True:
        dessine_mob()


def modifie():
    """
    modifie les objets de l'animation
    """
    if mob is True:
        mob_suit_souris()
    modifie_liste()


pyxel.init(LARG, HAUT, title="Simliste")
pyxel.mouse(True)
pyxel.run(modifie, dessine)
