"""
base pour des « jeux » d'algorithmique sur des listes
"""

from random import randint
import pyxel
# CONSTANTES
LARG = 256
HAUT = 128
COTE = 8                  # coté d'un élément
DEC = 2                   # décalage pour l'étiquette
SELECTB = pyxel.MOUSE_BUTTON_LEFT
VPOS_ELEMENTS = HAUT // 2
VPOS_INDICES = VPOS_ELEMENTS + 10

# représentation d'une liste
nbelm = 8                # nombre d'éléments dans la liste
listx = [i * LARG / nbelm + 1 for i in range(nbelm)]
listy = [VPOS_ELEMENTS] * nbelm
listv = [str(randint(0, 9)) for _ in range(nbelm)]

# élément déplaçable
mob = False
mobx = 0
moby = 0
mobv = False

# variable transitoire
transx = LARG // 2
transy = 3 * HAUT // 4
transv = ""


def interieur(rectx, recty, rectlg, rectht, pointx, pointy):
    """
    renvoie True si (pointx, pointy) est à l'intérieur du rectangle de
    coordonnées rectx, recty, rectlg, rectht, False autrement
    """
    return rectx <= pointx <= rectx + rectlg and \
        recty <= pointy <= recty + rectht


def abcisse_vers_index(abcisse):
    """
    renvoie l'index de l'élément de liste contenant le point (px, py) ou
    None si le point n'est pas au bon endroit
    """
    return int(abcisse // (LARG / nbelm))


def mob_suit_souris():
    global mobx, moby
    mobx, moby = pyxel.mouse_x, pyxel.mouse_y


def clic_gauche():
    """
    suite à l'appui du bouton gauche de la souris, créer un élément mobile,
    et lui donner la valeur de l'emplacement où se trouve la souris.
    """
    global mob, mobv
    if mob:
        return
    if transv != "" and \
       interieur(transx, transy, COTE+2, COTE+2, pyxel.mouse_x, pyxel.mouse_y):
        mobv = transv
    elif VPOS_ELEMENTS - COTE <= pyxel.mouse_y <= VPOS_ELEMENTS + COTE:
        mobv = listv[abcisse_vers_index(pyxel.mouse_x)]
    else:
        return
    mob = True
    mob_suit_souris()
    pyxel.mouse(False)


def clic_droit():
    """
    suite à l'appui du bouton droit de la souris, désactiver l'élément mobile
    et écrire sa valeur à l'emplacement où se trouvait la souris.
    """
    global mob, transv
    if not mob:
        return
    if interieur(transx, transy, COTE+2, COTE+2, pyxel.mouse_x, pyxel.mouse_y):
        transv = mobv
    elif VPOS_ELEMENTS - COTE <= pyxel.mouse_y <= VPOS_ELEMENTS + COTE:
        listv[abcisse_vers_index(mobx)] = mobv
    else:
        return
    mob = False
    pyxel.mouse(True)


def dessine_mob():
    pyxel.text(mobx + DEC, moby + DEC, str(mobv), 7)


def dessine_liste():
    for i in range(nbelm):
        pyxel.rect(listx[i], listy[i], COTE, COTE, 12)
        pyxel.text(listx[i] + DEC, listy[i] + DEC, str(listv[i]), 0)
        pyxel.text(listx[i] + DEC, VPOS_INDICES, str(i), 11)

def dessine_trans():
    pyxel.text(transx + DEC, transy + DEC, str(transv), 7)
    pyxel.rectb(transx, transy, COTE + 2, COTE + 2, 7)


def repondre_boutons():
    if pyxel.btnr(pyxel.MOUSE_BUTTON_LEFT):
        clic_gauche()
    if pyxel.btnr(pyxel.MOUSE_BUTTON_RIGHT):
        clic_droit()


def texte_explicatif():
    pyxel.text(2, 2, "left click : catch and move selected value", 7)
    pyxel.text(2, 12, "right click : put and write value at selected place", 7)

def dessine():
    """
    dessine les éléments graphiques
    """
    pyxel.cls(0)
    texte_explicatif()
    dessine_liste()
    dessine_trans()
    if mob is True:
        dessine_mob()


def modifie():
    """
    modifie les objets de l'animation
    """
    if mob is True:
        mob_suit_souris()
    repondre_boutons()


pyxel.init(LARG, HAUT, title="Simliste")
pyxel.mouse(True)
pyxel.run(modifie, dessine)
